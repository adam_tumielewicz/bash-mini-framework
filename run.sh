#!/bin/bash


function init() {
    # Stop on Error
    set -e
    # Stop on undefined
    set -u
    # Debug trace
    # set -x

    local absSelf="$(readlink -f "${BASH_SOURCE[0]}")"
    local absSelfDir="$(dirname "${absSelf}")"

    source "${absSelfDir}/bootstrap.sh"

	registerToolWorkDir "${absSelfDir}"
    registerPool "${absSelfDir}/modules"
    bootstrap
}


function main() {
    init

	routeCommand "${@}"
}


main "${@}"
