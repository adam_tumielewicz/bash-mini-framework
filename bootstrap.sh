#!/bin/bash

function bootstrap() {
    registerModules
    loadModules
    registerCommandOwner
}

function registerModules() {
    for pool in $(modulesPools); do
        for module in $(find "${pool}" -type f -follow -iname "*.sh"); do
            modules="${modules:-}${modules:+ }source ${module};"
        done
    done
}

function registerPool() {
    local pool="$( readlink -f "${1}" )"

	mkdir -p "${pool}" &> /dev/null || true

    modulePools="${modulePools:-}${modulePools:+ }${pool}"
}

function loadModules() {
    eval "${modules:-}"
}

function modulesPools() {
    echo "${modulePools}"
}

function commandsPools() {
	echo "$(modulesPools)/commands"
}

function registerToolWorkDir() {
	local path="$( readlink -f "${1}" )"

	baseDir="${path}"
	registerWorkDir "${path}/storage"
	registerCacheDir "${path}/storage"
}

function baseDir() {
	echo "${baseDir}"
}

function registerWorkDir() {
	local storagePath="${1}"

	workDir="${storagePath}/.workdir"

	if [[ ! -d "${workDir}" ]]; then
		mkdir -p "${workDir}"
	fi
}

function registerCacheDir() {
	local storagePath="${1}"

	cacheDir="${storagePath}/.cache"

	if [[ ! -d "${cacheDir}" ]]; then
		mkdir -p "${cacheDir}"
	fi
}

function inventoryCacheDir() {
	echo "${cacheDir}"
}

function registerCommandOwner() {
	export ACTION_OWNER="${1:-default}"
}