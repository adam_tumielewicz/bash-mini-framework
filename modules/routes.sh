#!/bin/bash
function legalCommands() {
	echo "$(commandsRoutes)"
}

function isCommandLegal() {
	local command="${1}"

	echo "$(legalCommands)" | grep -q -w "${command}"
}

function routeCommand() {
	if [[ ${#} == 0 ]]; then
		illegalParameterError
    fi

	local routeCommand="$(basename "${0}")"
	if isCommandLegal "${routeCommand}"; then
		"${routeCommand}" "${@}"
		exit 0
	fi

	routeCommand="${1}"
	shift 1
	if isCommandLegal "${routeCommand}"; then
		"${routeCommand}" "${@}"
		exit 0
	fi

	illegalParameterError "${routeCommand}"
}

function commandsRoutes() {
	for command in $(ls -A1 $(commandsPools)); do
		commandList="${commandList:-}${commandList:+} ${command%.sh} "
	done

	echo "${commandList}"
}

function printUsage() {
	local bold=$(tput bold)
	local normal=$(tput sgr0)

	echo "	"
	echo "#############################################################"
	echo "##############  ${bold}OPENLDAP MANAGER COMMAND HELP${normal}  ##############"
	echo "#############################################################"
	echo "	"
	echo "Description:"
	"${ACTION_OWNER}-usage"
	echo "	"
}

