#!/bin/bash

function action() {
	(
		fireCommandAction "${FUNCNAME[0]}" "${BASH_SOURCE}" "${@}"
	)
}

function action-default() {
	printCommandHelp "${BASH_SOURCE}"
}

action-usage() {
	echo "Action command help"
}