#!/bin/bash

function commandInterface() {
	local sourceFilePath="${1}"

	grep "^function" "${sourceFilePath}"
}

function isCommandExists() {
	local command="${1}"

	if ! type "${command}" &> /dev/null; then
		illegalParameterError "${command}"
	fi
}

function fireCommandAction() {
	local actionOwner="${1}"
	local interfaceSource="${2}"
	shift 2

	registerCommandOwner "${actionOwner}"

	local command="${1:-}"
	if [[ -z "${command}" ]]; then
		"${actionOwner}-default"
		exit 0
	fi

	shift 1
	local commandAction=""
	commandAction="${actionOwner}-${command}"

	if isCommandValid "${commandAction}" "${interfaceSource}"; then
		"${actionOwner}-${command}" "${@}"
		exit 0
	fi
}

function isCommandValid() {
	local command="${1}"
	local interfaceSource="${2}"

	if ! commandInterface "${interfaceSource}" | grep -q -w "${command}"; then
		illegalParameterError "${command}"
		exit 1
	fi
}

function printCommandHelp() {
	local interfaceSource="${1}"

	local bold=$(tput bold)
	local normal=$(tput sgr0)

	echo "	"
	echo "#############################################################"
	echo "##############  ${bold}OPENLDAP MANAGER COMMAND HELP${normal}  ##############"
	echo "#############################################################"
	echo "	"
	echo "Description:"
	"${ACTION_OWNER}-usage"
	echo "	"
	echo "	Available commands in openLDAP for ${bold}'${ACTION_OWNER}'${normal} scope: "
	echo "	"
	for command in $(commandInterface "${interfaceSource}" | cut -d ' ' -f2); do
		[[ "${command%\(\)}" == "${ACTION_OWNER}" ]] || [[ "${command%\(\)}" == "${ACTION_OWNER}-default" ]] && continue
		command="$(parseCommandHelpFormat ${command%\(\)})"
		echo "	${bold}${command}${normal}"
	done
}

function default-usage() {
	echo "Available commands: ${bold}'$(legalCommands)'${normal}"
}

function parseCommandHelpFormat() {
	local rawName="${1}"

	echo "${rawName}" | cut -d '-' -f2
}