#!/bin/bash

function collectionMap() {
	local callback="${1}"
	local collectionGetMethod="${2}"
	shift 2

	local itemCollection="$(eval ${collectionGetMethod})"

	for item in ${itemCollection}; do
		[[ -z "${item}" ]] && continue;
		"${callback}" "${item}" "${@}"
	done
}

function illegalParameterError() {
	local command="${1:-}"
	local errorMsg="${2:-Illegal number of parameters with command '${command}'}"

	echo "${errorMsg}"
	printUsage
	exit 1
}